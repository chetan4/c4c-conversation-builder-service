#########################################################################
# Sample Tweet Object returned
# {"id"=>"tag:search.twitter.com,2005:570883831655346177", "objectType"=>"activity", "actor"=>{"objectType"=>"person", "id"=>"id:twitter.com:2968717471", "link"=>"http://www.twitter.com/twitabc4ya", "displayName"=>"twitabc4ya", "postedTime"=>"2015-01-09T09:00:44.000Z", "image"=>"https://abs.twimg.com/sticky/default_profile_images/default_profile_6_normal.png", "summary"=>nil, "links"=>[{"href"=>nil, "rel"=>"me"}], "friendsCount"=>42, "followersCount"=>2, "listedCount"=>0, "statusesCount"=>29, "twitterTimeZone"=>nil, "verified"=>false, "utcOffset"=>nil, "preferredUsername"=>"twitabc4ya", "languages"=>["en"], "favoritesCount"=>0}, "verb"=>"post", "postedTime"=>"2015-02-26T09:51:26.000Z", "generator"=>{"displayName"=>"c4c-staging", "link"=>"http://c4cstaging.idyllic-software.com"}, "provider"=>{"objectType"=>"service", "displayName"=>"Twitter", "link"=>"http://www.twitter.com"}, "link"=>"http://twitter.com/twitabc4ya/statuses/570883831655346177", "body"=>"@thedirtycoder nice", "object"=>{"objectType"=>"note", "id"=>"object:search.twitter.com,2005:570883831655346177", "summary"=>"@thedirtycoder nice", "link"=>"http://twitter.com/twitabc4ya/statuses/570883831655346177", "postedTime"=>"2015-02-26T09:51:26.000Z"}, "inReplyTo"=>{"link"=>"http://twitter.com/thedirtycoder/statuses/569339014085517312"}, "favoritesCount"=>0, "twitter_entities"=>{"hashtags"=>[], "trends"=>[], "urls"=>[], "user_mentions"=>[{"screen_name"=>"thedirtycoder", "name"=>"Uncle Bob's Nephew", "id"=>1447364456, "id_str"=>"1447364456", "indices"=>[0, 14]}], "symbols"=>[]}, "twitter_filter_level"=>"low", "twitter_lang"=>"en", "retweetCount"=>0, "gnip"=>{"matching_rules"=>[{"value"=>"in_reply_to_status_id:569339014085517312", "tag"=>"569339014085517312"}], "klout_score"=>11}}
################################################################################

require 'json'
require_relative "populate"
require_relative "tweet_user"

class Tweet
	include Populate::Tweet

	attr_accessor :rules,
							  :lang,
							  :id,
							  :tweet_type,
							  :tweet_user,
							  :posted_at,
							  :provider,
							  :tweet_link,
							  :tweet_body,
							  :favorites_count,
							  :in_reply_to_status_id,
							  :retweets_count,
							  :hashtags,
							  :user_mentions,
							  :urls




	def initialize(json_tweet)		
		@id = populate_id(json_tweet)
		@tweet_type = populate_tweet_type(json_tweet) 
		@tweet_user = populate_tweet_user(json_tweet) 
		@posted_at = populate_posted_at(json_tweet) 
		@provider = populate_provider(json_tweet)
		@tweet_link = populate_tweet_link(json_tweet)
		@tweet_body = populate_tweet_body(json_tweet)
		@in_reply_to_status_id = populate_in_reply_to_status_id(json_tweet)
		@retweets_count = populate_retweets_count(json_tweet)
		@favorites_count = populate_favorites_count(json_tweet)
		@hashtags = populate_hash_tags(json_tweet)
		@user_mentions = populate_user_mentions(json_tweet)
		@urls = populate_urls(json_tweet)
		@lang = populate_lang(json_tweet)
		@rules = populate_gnip_rules(json_tweet)	
		@images = populate_tweet_images(json_tweet)	
	end

	def has_parent?
		self.in_reply_to_status_id.present?
	end

	def to_json
		{
			id: self.id,
			tweet_type: self.tweet_type,
			tweet_user: self.tweet_user.to_json,
			posted_at: self.posted_at,
			provider: self.provider,
			tweet_link: self.tweet_link,
			tweet_body: self.tweet_body,
			in_reply_to_status_id: self.in_reply_to_status_id,
			retweets_count: self.retweets_count,
			favorites_count: self.favorites_count,
			hash_tags: self.hashtags,
			user_mentions: self.user_mentions,
			urls: self.urls,
			lang: self.lang,
			rules: self.rules			
		}
	end	
end
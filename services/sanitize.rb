require 'json'

module Sanitize
	extend self
	def sanitize(job)
		job.strip		
	end

	def parse(job)
		JSON.parse(job)
	end
end
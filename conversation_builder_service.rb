require 'conversation_builder.rb'
require './services/job.rb'
require './services/worker.rb'
require './services/conversation_builder_api_service.rb'


java_import 'java.util.concurrent.FutureTask'
java_import 'java.util.concurrent.ThreadPoolExecutor'
java_import 'java.util.concurrent.TimeUnit'
java_import 'java.util.concurrent.LinkedBlockingQueue'

class ConversationBuilderService
	attr_accessor :workers, :jobs, :thread_pool_executor

	def initialize(options = {})
		@options = options	
		@workers = []
		@jobs = []		
		@workers_to_jobs = {}				
		@thread_pool_executor = ThreadPoolExecutor.new(8, 16, 180, TimeUnit::SECONDS, LinkedBlockingQueue.new)
	end

	def start
		begin
			puts "Starting conversation builder service..."
			@working = true
			start_service_loop
		rescue => e
			puts "#{e.message} #{e.backtrace}"
		end		
	end	

	def start_service_loop
		puts "Starting some work..."
		
		@thread = Thread.new do 
			while @working do 
				current_job = initialize_next_job
				process_job(current_job)
			end
		end
	end

	def process_job(job)
		thread_pool_executor.execute(job)
	end

	def stop
		@working = false
		puts "Stopping service..."
		@thread.join
		thread_pool_executor.shutdown()
		puts "Stopped service"
	end

	def initialize_next_job
		worker_job = Worker.new(Job.next)	
		puts "Worker initialized....#{Time.now}"	
		worker_job = FutureTask.new(worker_job)
		puts "Future Task prepared....#{Time.now}"	
		jobs.push(worker_job)
		worker_job
	end
end
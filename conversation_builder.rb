require 'rubygems'
require 'bundler/setup'
Bundler.setup()
#require File.join('./lib/services/conversation_builder_service.rb')
require 'yaml'
require 'connection_pool'
require 'pry'
require 'torquebox'
require 'torquebox/logger'
require 'active_support/all'
require 'openssl'
require 'redis'

ENV['SSL_CERT_DIR'] = '/usr/lib/ssl/'

Dir.glob("./config/initializers/*").sort().each do |file|
	require file
end

require_relative './services/sanitize.rb'
require_relative './services/job.rb'
require_relative './services/worker.rb'
require_relative './services/tweet_user.rb'
require_relative './services/tweet.rb'
require_relative './services/conversation_builder_api_service.rb'

puts "All files were required..."


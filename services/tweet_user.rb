require 'json'
require_relative 'populate.rb'

class TweetUser
	include Populate::Tweeter

	attr_accessor :username,
								:profile_link,
								:id,
								:image,
								:profile_summary,
								:friends_count,
								:followers_count,
								:listed_count,
								:statuses_count,
								:twitter_time_zone,
								:verified,
								:favorites_count

	def initialize(tweet_user_json)
		@username = populate_username(tweet_user_json)
		@profile_link = populate_profile_link(tweet_user_json)
		@id = populate_id(tweet_user_json)
		@image = populate_profile_image(tweet_user_json)
		@profile_summary = populate_profile_summary(tweet_user_json)
		@friends_count = populate_friends_count(tweet_user_json)
		@followers_count = populate_followers_count(tweet_user_json)
		@listed_count = populate_listed_count(tweet_user_json)
		@statuses_count = populate_listed_count(tweet_user_json)
		@twitter_time_zone = populate_twitter_time_zone(tweet_user_json)
		@verified = populate_verified(tweet_user_json)
		@favorites_count = populate_favorites_count(tweet_user_json)
	end

	def to_json
		{
			username: self.username,
			profile_link: self.profile_link,
			id: self.id,
			image: self.image,
			profile_summary: self.profile_summary,
			friends_count: self.friends_count,
			followers_count: self.followers_count,
			listed_count: self.listed_count,
			statuses_count: self.statuses_count,
			twitter_time_zone: self.twitter_time_zone,
			verified: self.verified,
			favorites_count: self.favorites_count
		}
	end	
end
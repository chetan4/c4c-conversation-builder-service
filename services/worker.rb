require 'json'
java_import 'java.util.concurrent.Callable'

class Worker
	include Callable

	attr_accessor :job
	
	def initialize(job)		
		@job = job
	end

	def call
		begin
			puts "Executing Job.....#{job.inspect}"
			santized_job = santize_job(job)
			hashed_job = parse(santized_job)
			tweet = Tweet.new(hashed_job)
			puts "Triggering conversation builder...."
			conversations = ConversationBuilderApiService.new(tweet).generate_conversations
			puts "Conversation received...."
			publish_conversation(conversations.to_json)
		rescue => e
			puts "#{e.message} #{e.backtrace}"			
		end
	end

	def santize_job(job)
		job.strip		
	end

	def parse(job)
		JSON.parse(job)
	end	

	def publish_conversation(conversations_json)		
		REDIS_CONNECTION_POOL.with do |conn|
			conn.lpush(CHANNEL_CONFIG['conversations'], conversations_json)

			puts "Published a conversation to the /conversations channel"
		end
	end
end
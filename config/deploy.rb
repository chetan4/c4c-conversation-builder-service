# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'c4c-conversation-builder'
set :repo_url, ' git@github.com:idyllicsoftware/c4c-conversation-builder-service.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/srv/apps/c4c-conversation-builder'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5
set :rvm_type, :user 
set :rvm_ruby_version, 'jruby-1.7.13@torquebox' 
set :bundle_flags, '--deployment'

namespace :deploy do
  #after :finished, :clear_cache do
  desc "deploys code"
  task :deploy_code do 
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      within current_path do
      	  puts "Restarting torquebox......................."
      	  execute "source ~/.bashrc"
      	  execute "echo $JBOSS_HOME"      	  
      	  execute :rake, "torquebox:deploy #{current_path}"
      end
    end
  end

  desc "Restarts torquebox"
  task :restart_torquebox do
  	on roles(:web) do 
  		within current_path do   			      
        #execute '/etc/init.d/torquebox stop'
        #sleep(2)
        execute '/etc/init.d/torquebox start'
  		end
  	end
  end
end

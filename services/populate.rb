module Populate
	module Tweet
		def populate_id(json_tweet)
			json_tweet['id'].split(':').last
		end

		def populate_tweet_type(json_tweet)
			json_tweet['objectType']
		end

		def populate_tweet_user(json_tweet)
			TweetUser.new(json_tweet['actor'])
		end

		def populate_posted_at(json_tweet)
			json_tweet['postedTime']
		end

		def populate_provider(json_tweet)
			provider = json_tweet['provider']
			return provider['displayName'].downcase if provider and provider['displayName']
			nil		
		end

		def populate_tweet_link(json_tweet)
			json_tweet['link']
		end

		def populate_tweet_body(json_tweet)
			json_tweet['body']
		end

		def populate_in_reply_to_status_id(json_tweet)
			in_reply_to = json_tweet['inReplyTo']
			
			if in_reply_to && in_reply_to['link']
				in_reply_to_status_link = in_reply_to['link']
				status_id = parse_status_link_for_id(in_reply_to_status_link)
				return status_id
			end

			nil
		end

		def parse_status_link_for_id(link)
			link.strip.split('/').last
		end

		def populate_favorites_count(json_tweet)
			json_tweet['favoritesCount'].to_i
		end

		def populate_retweets_count(json_tweet)
			json_tweet['retweetCount'].to_i
		end

		def populate_hash_tags(json_tweet)
			if json_tweet['twitter_entities'] && json_tweet['twitter_entities']['hashtags']
				return json_tweet['twitter_entities']['hashtags']
			end

			return []
		end

		def populate_user_mentions(json_tweet)
			return [] if !(json_tweet['twitter_entities'] && json_tweet['twitter_entities']['user_mentions'])
			user_mentions = json_tweet['twitter_entities']['user_mentions']
			
			return (user_mentions.map do |um| 
							{
								username: um['screen_name'],
								name: um['name'],
								id: um['id_str']
							}
						end)		
		end

		def populate_urls(json_tweet)	
			[] if !(json_tweet['twitter_entities'] && json_tweet['twitter_entities']['urls'])
			urls = json_tweet['twitter_entities']['urls']

			return urls.map do |tu|
								{
									expanded_url: tu['expanded_url'],
									short_url: tu['short_url']
								}
						 end
		end

		def populate_lang(json_tweet)
			json_tweet['twitter_lang']
		end

		def populate_gnip_rules(json_tweet)
			matching_rules = nil

			if (json_tweet['gnip']['matching_rules'].present?)
				matching_rules = json_tweet['gnip']['matching_rules'].map do |rule|
						rule['value']
				end
			end

			klout_score = json_tweet['gnip']['klout_score']
			
			return {
				matching_rules: matching_rules,
				klout_score: klout_score
			}
		end

		def populate_tweet_images(json_tweet)
			return [] if !(json_tweet['twitter_entities'] && json_tweet['twitter_entities']['media'])

			tweeted_images = ThreadSafe::Array.new
			tweet_images = json_tweet['twitter_entities']['media']
			tweet_images.each do |ti|
				image = {
					id: ti['id'],
					media_url: ti['media_url'],
					media_url_https: ti['media_url_https'],
					url: ti['url'],
					display_url: ti['display_url'],
					expanded_url: ti['expanded_url']
				}

				tweeted_images.push(ti)
			end

			tweeted_images
		end
	end

	module Tweeter
		def populate_username(tweet_user_json)
			tweet_user_json['preferredUsername'] rescue nil
		end

		def populate_profile_link(tweet_user_json)
			tweet_user_json['link'] rescue nil
		end

		def populate_id(tweet_user_json)
			tweet_user_json['id'].split(":").last rescue nil
		end

		def populate_profile_image(tweet_user_json)
			tweet_user_json['image']
		end

		def populate_profile_summary(tweet_user_json)
			tweet_user_json['summary']
		end

		def populate_friends_count(tweet_user_json)
			tweet_user_json['friendsCount'].to_i
		end

		def populate_followers_count(tweet_user_json)
			tweet_user_json['followersCount'].to_i
		end

		def populate_listed_count(tweet_user_json)
			tweet_user_json['listedCount'].to_i
		end

		def populate_statuses_count(tweet_user_json)
			tweet_user_json['statusesCount'].to_i
		end

		def populate_twitter_time_zone(tweet_user_json)
			tweet_user_json['twitterTimeZone']
		end

		def populate_verified(tweet_user_json)
			tweet_user_json['verified']
		end

		def populate_favorites_count(tweet_user_json)
			tweet_user_json['favoritesCount']
		end
	end	
end
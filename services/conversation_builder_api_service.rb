require_relative 'sanitize'
require 'net/https'
require 'uri'

class ConversationBuilderApiService
	include Sanitize

	attr_accessor :tweet,
								:root_tweet_id,
								:conversation_queue 

	def initialize(tweet)		
		@tweet = tweet			
		@root_tweet_id = nil
		@conversation_queue = ThreadSafe::Array.new			
		@conversation_queue.push(tweet)
	end

	def conversation_length
		@conversation_queue.length
	end

	def generate_conversations		
		current_tweet = self.tweet

		while (current_tweet.has_parent?) do			
			parent_tweet = get_parent(current_tweet)
			save_top_of_conversation_queue(parent_tweet)
			current_tweet = parent_tweet
		end

		return conversation_queue	
	end

	def get_parent(current_tweet)
		fetch_tweet(current_tweet)
	end

	def save_top_of_conversation_queue(parent_tweet)
		@conversation_queue.unshift(parent_tweet)
	end

	#TODO: Find a way to indicate deleted tweets.
	def fetch_tweet(current_tweet)
		parent_tweet = fetch(current_tweet)		
		sanitized_tweet = sanitize(parent_tweet)		
		tweet_hash = parse(sanitized_tweet).first
		
		if (tweet_hash['available'])			
			Tweet.new(tweet_hash['content'])
		else
			nil 
		end
	end

	private

	# tweet_ids = "?ids=575091094943309826"
	def fetch(current_tweet)
		begin
			tweet_ids = "?ids=#{current_tweet.in_reply_to_status_id}"
			uri = URI.parse(GNIP_API_CONFIG['url'] + tweet_ids)
			http = Net::HTTP.new(uri.host, uri.port)
			http.use_ssl = true
			#cert = OpenSSL::X509::Certificate.new(pem_file)
			#http.cert = cert
			http.verify_mode = OpenSSL::SSL::VERIFY_PEER
			request = Net::HTTP::Get.new(uri.request_uri)
			request.basic_auth("#{GNIP_API_CONFIG['username']}", "#{GNIP_API_CONFIG['password']}")	
			puts "Request information === #{uri.host} #{uri.port} #{uri.path}"		
			response = http.request(request)		
			puts "Response status: #{response.code}"
			response.body
		rescue => e
			puts "[Error] #{e.message} #{e.backtrace}"
			raise e
		end
	end
end
configurations = YAML.load_file(File.join("config", "redis.yml"))
puts "Configurations: #{configurations.inspect}"
ENV['SERVICE_ENV'] = 'development' unless(ENV['SERVICE_ENV'])

REDIS_CONFIG = configurations[ENV['SERVICE_ENV']]
puts REDIS_CONFIG

REDIS_CONNECTION_POOL = ConnectionPool.new(size: 30, timeout: 5) {
	Redis.new(REDIS_CONFIG)
}

puts "Redis Connection Pool: #{REDIS_CONNECTION_POOL}"




class Job
	attr_accessor :job_channel, :job, :queue
	
	@@mutex = Mutex.new

	def self.next
		@@mutex.synchronize {
			puts "Next job fetch requested.............."
			job = Job.new('tweets')
			job.fetch_job
			job.job
		}
	end

	def initialize(channel = 'tweets')
		@queue = CHANNEL_CONFIG[channel.to_s]				
		@job_channel = CHANNEL_CONFIG[channel.to_s]
		puts "CHANNEL_CONFIG #{CHANNEL_CONFIG.inspect}"
		@job = nil
	end

	def fetch_job
		puts "Fetching Job from channel #{job_channel.to_s}................"
		REDIS_CONNECTION_POOL.with do |conn|
			@queue, @job = conn.brpop(job_channel)	
			puts "Job fetched on QUEUE: #{@queue} with Job Payload: #{@job.inspect}"
		end		
	end
end